<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_forum?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'اين كليدواژه در اين زبان به هيچ پيامي چسبيده نيست.',
	'aucune_reponse' => 'بدون پاسخ',
	'avertissement_code_forum' => 'براي گنجاندن كد يا مقدار مورد علاقه خود، مي‌توانيد از ميان‌بر‌هاي متن‌ نگاري زير استفاده كنيد :<ul><li>&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... code ayant des lignes très longues ...&lt;/cadre&gt;</li></ul>  ',
	'avertissementforum' => 'پ.ن <b>N.B.</b>سخنگاه‌هاي اين سايت خيلي فعال هستند. تشكر فراوان از كساني كه به اين عرصه‌‌ي كمك‌هاي متقابل روح و عني بخشيدند.<p> با اين حال، هرچه سخنگاه فعال‌تر، پيگيري و مراجعه به آن نيز مشكل تر. براي تبديل اين سخنگاه‌ها به يك تجربه‌ي واقعاً‌ لذت بخش بسيار خرسند مي‌شويم كه به جند توصيه توجه كنيد:<br/>  <img src=\'puce.gif\' border=\'0\' /> پيش از شروع يك گفتگوي جديد، مطمئن شويد كه موضوع در اينجا پيش‌ تر بررسي نشده باشد؛‌<br /><img src=\'puce.gif\' border=\'0\' /> مطمئن شويد كه پرسش را در بخش مربوطه مطرح مي‌كنيد. ',
	'avertissementtitre' => '<p>مطمئن شويد كه <strong> يك عنوان دقيق به پرسش خود داده باشيد <strong> تا به ساير بازديدكنندگان فرصت جستجو در سخنگاه‌ها را بدهد.</p> <p><strong> پيام‌هاي فاقد عنوان دقيق حذف مي‌شوند.</strong></p> ',

	// B
	'barre_cadre_html' => 'كادر بندي و رنگ دهي به <cadre class=\'html4strict\'>du code html</cadre>',
	'barre_cadre_php' => 'كادربندي و رنگ دهي به <cadre class=\'php\'>du code php</cadre>',
	'barre_cadre_spip' => 'كادر بندي و رنگ دهي به <cadre class=\'spip\'>du code spip</cadre>',
	'barre_code' => 'كنجاندن &lt;code&gt;du code&lt;/code&gt;',
	'barre_inserer_code' => 'گنحاندن، كادر كردن، و رنگ دادن به كد ',
	'barre_quote' => ' نقل &lt;quote&gt;un message&lt;/quote&gt;',

	// C
	'classer' => 'طبقه‌بندي',
	'clos' => 'اين خط بحث بسته مي‌شود',

	// D
	'deplacer_dans' => ' جاسازي در ',
	'derniers' => 'واپسين پيام',
	'download' => 'بارگذاري واپسين ويرايش ',

	// F
	'forum_attention_explicite' => 'اين تيتر به اندازه‌ي كافي روشن نيست، بيشتر توضيح دهيد:', # MODIF
	'forum_invalide_titre' => 'اين خط پيام‌ بي اعتبار است',
	'forum_votre_email' => 'نشاني ايميل شما (اگر ميل به دريافت پاسخ داريد):',

	// G
	'galaxie' => 'در جهان اسپيپ ',

	// I
	'info_ajouter_document' => 'مي‌توانيد يك كپي از اكران به پيام خود بچسبانيد',
	'info_connexion' => 'به شما اجازه مي‌دهد پيام خود را طي يك ساعت ويرايش كنيد',
	'info_tag_forum' => 'مي‌توانيد اين صفحه‌ي سخنگاه را با مناسب‌ترين كليدواژه‌ها برچسب كنيد. اين به بازديدكنندگان آي كمك مي‌كند تا پاسخ‌ها را فوري‌تر بيابند. ',
	'interetquestion' => 'ميل خود به اين پرسش را نشان دهيد',
	'interetreponse' => 'ميل خود را به اين پاسخ نشان دهيد',
	'inutile' => 'بي‌فايده',

	// L
	'liens_utiles' => 'پيوندهاي مفيد',
	'login_login2' => 'لاگين',

	// M
	'meme_sujet' => 'روي همان سرفصل ',
	'merci' => 'تشكر',
	'messages' => 'پيام‌ها',

	// N
	'navigationrapide' => 'ناوبري سريع:', # MODIF
	'nouvellequestion' => 'يك پرسش جديد طرح كنيد',
	'nouvellereponse' => 'پاسخ به پرسش',

	// P
	'page_utile' => 'اين صفحه را يافتيد:',
	'par_date' => 'طبق تاريخ ',
	'par_interet' => 'بر اساس سرفصل',
	'par_pertinence' => 'بر اساس ارتباط ',

	// Q
	'questions' => 'پرسش‌ها',
	'quoideneuf' => 'اصلاحات اخير',

	// R
	'rechercher' => 'جستحو',
	'rechercher_forums' => 'جستجو در سخنگاه‌ها',
	'rechercher_tout_site' => 'تمام سايت',
	'reponses' => 'پاسخ(ها)',
	'resolu' => 'حل شده',
	'resolu_afficher' => 'اول نمايش پيام‌هاي پيوندشده به كليدواژه «حل شده» ',
	'resolu_masquer' => 'پنهان كردن پيام‌هاي پيوند شده به كليدواژه «حل شده»‌', # MODIF

	// S
	'suggestion' => 'پيش از ادامه،‌آيا صفحه‌هاي زير را بررسي كرده‌ايد؟ شايد پاسخي را كه مي‌خواهيد داشته باشند.',
	'suivi_thread' => 'مشترك سازي اين خط سخنگاه',

	// T
	'thememessage' => 'تم اين سخنگاه :',
	'toutes_langues' => 'در تمام زبان‌ها',
	'traductions' => 'ترجمه‌هاي اين متن :',

	// U
	'utile' => 'مفيد'
);
