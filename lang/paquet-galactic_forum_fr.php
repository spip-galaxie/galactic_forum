<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/galactic_forum.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'galactic_spip_net_description' => '',
	'galactic_spip_net_nom' => 'Galactic - forum.spip.net',
	'galactic_spip_net_slogan' => 'Surcharge le squelette Galactic'
);
