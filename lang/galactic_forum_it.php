<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_forum?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Questa parola chiave non è collegata ad alcun messaggio in questa lingua.',
	'aucune_reponse' => 'Nessuna risposta',
	'avertissement_code_forum' => 'Per inserire parte di codice o evidenziare le proprie soluzioni è possibile utilizzare le seguenti scorciatoie tipografiche:<ul><li>&lt;code&gt;... una o più linee di codice ...&lt;/code&gt;</li><li>&lt;cadre&gt;... codice con linee molto lunghe ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>N.B.</b> I forum di questo sito sono molto attivi. Ringraziamo tutti coloro che animano e arricchiscono questi spazi dedicati al confronto e allo scambio reciproco.<p>Tuttavia, più un forum è attivo e più diventa difficile la sua consultazione. Al fine di rendere questi forum più fruibili vi preghiamo di seguire queste raccomandazioni :<br /><img src=\'puce.gif\' border=\'0\' /> prima di iniziare un nuovo argomento di discussione, verificare che tale argomento non sia stato già trattato ;<br /><img src=\'puce.gif\' border=\'0\' /> assicurarsi di porre la domanda nella rubrica appropriata.',
	'avertissementtitre' => '<b>Assicurarsi di dare <font color=’red’>un titolo esplicito alla propria domanda</font> per facilitare la navigazione successiva agli altri visitatori dei forum.</b><p><font color=’red’>I messaggi che non hanno un titolo esplicito verrranno cancellati.</font>',

	// B
	'barre_cadre_html' => 'Riquadrare e colorare <cadre class=\'html4strict\'>del codice html</cadre>',
	'barre_cadre_php' => 'Riquadrare e colorare <cadre class=\'php\'>del codice php</cadre>',
	'barre_cadre_spip' => 'Riquadrare e colorare <cadre class=\'spip\'>del codice spip</cadre>',
	'barre_code' => 'Inserire <code>del code</code>',
	'barre_inserer_code' => 'Inserire, riquadrare, colorare del codice',
	'barre_quote' => 'Citare <quote>un messaggio</quote>',

	// C
	'classer' => 'Ordina',
	'clos' => 'Questo filone di discussione è chiuso',

	// D
	'deplacer_dans' => 'Sposta in',
	'derniers' => 'Ultimi messaggi',
	'download' => 'Download dell’ultima versione',

	// F
	'forum_attention_explicite' => 'Questo titolo non è molto esplicito, si prega di precisare:', # MODIF
	'forum_invalide_titre' => 'Questo filone di messaggi è stato invalidato',
	'forum_votre_email' => 'Il tuo indirizzo email (se desideri ricevere le risposte):',

	// G
	'galaxie' => 'Nella galassia SPIP',

	// I
	'info_ajouter_document' => 'Puoi aggiungere uno screenshot al tuo messaggio',
	'info_connexion' => 'Consente di modificare il messaggio entro un’ora',
	'info_tag_forum' => 'È possibile etichettare questa pagina di forum con parole chiave che si ritengono importanti; esse permetteranno ai prossimi visitatori di orientarsi meglio nel sito.',
	'interetquestion' => 'Indicare il livello di interesse che si attribuisce a questa domanda',
	'interetreponse' => 'Indicare il livello di interesse che si attribuisce a questa risposta',
	'inutile' => 'inutile',

	// L
	'liens_utiles' => 'Link utili',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'Sullo stesso argomento',
	'merci' => 'grazie',
	'messages' => 'messaggi',

	// N
	'navigationrapide' => 'Navigazione rapida:', # MODIF
	'nouvellequestion' => 'Fare una nuova domanda',
	'nouvellereponse' => 'Rispondere alla domanda',

	// P
	'page_utile' => 'Questa pagina vi è stata:',
	'par_date' => 'per data',
	'par_interet' => 'per interesse',
	'par_pertinence' => 'per pertinenza',

	// Q
	'questions' => 'Domande',
	'quoideneuf' => 'Ultime modifiche',

	// R
	'rechercher' => 'Cerca',
	'rechercher_forums' => 'Cercare nei forum',
	'rechercher_tout_site' => 'tutto il sito',
	'reponses' => 'Risposte',
	'resolu' => 'Risolto',
	'resolu_afficher' => 'Mostra dapprima i messaggi collegati alla parola chiave « risolto »',
	'resolu_masquer' => 'Nascondi i messaggi collegati alla parola chiave « risolto »', # MODIF

	// S
	'suggestion' => 'Prima di continuare, hai consultato le seguenti pagine? Contengono forse la risposta a quello che cerchi.',
	'suivi_thread' => 'RSS di questo filone del forum',

	// T
	'thememessage' => 'Tema di questo forum:',
	'toutes_langues' => 'In tutte le lingue',
	'traductions' => 'Traduzioni di questo testo:',

	// U
	'utile' => 'utile'
);
