<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_forum?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aucun_message_mot' => 'Dieses Schlagwort wurde keinem Beitrag dieser Sprache zugeordnet.',
	'aucune_reponse' => 'Keine Antwort',
	'avertissement_code_forum' => 'Um Code einzufügen oder Ihre Lösungen hervorzuheben, können Sie folgende Kürzel verwenden:<ul><li>&lt;code&gt;... eine oder mehrere Zeilen Code ...&lt;/code&gt;</li><li>&lt;cadre&gt;... Code mit sehr langen Zeilen ...&lt;/cadre&gt;</li></ul>',
	'avertissementforum' => '<b>N.B.</b> Die Foren dieser Website sind sehr aktiv. Vielen Dank an alle, die sich hier engagieren und Hilfestellungen geben.<p>Bitte denken Sie daran, dass Forendurch eine grosse Zahl an Beiträgen leicht unübersichtlich werden. Vielen Dank dafür, dass Sie die folgenden Hinweis beachten:<br /><img src=\'puce.gif\' border=\'0\' /> Bevor Sie ein neues Thema beginnen, prüfen Sie bitte, ob es nicht bereits behandelt worden ist.<br /><img src=\'puce.gif\' border=\'0\' /> Bitte achten Sie darauf, dass Sie Ihre Frage in der dafür vorgesehenen Rubrik stellen.',
	'avertissementtitre' => 'Bitte denken Sie daran, <font color=’red’>Ihrer Frage einen aussagekräftigen Titel zu geben</font>, damit die anderen Leser sich leichter in den Foren orientieren können.<p><font color=’red’>Einträge ohne Titel werden gelöscht.</font>',

	// B
	'barre_cadre_html' => 'Rahmen und Farben <cadre class=\'html4strict\'>des HTML-Code</cadre>',
	'barre_cadre_php' => 'Rahmen und Farben <cadre class=\'php\'>des PHP-Code</cadre>',
	'barre_cadre_spip' => 'Rahmen und Farben <cadre class=\'spip\'>des SPIP-Code</cadre>',
	'barre_code' => '&lt;code&gt;Code&lt;/code&gt; einfügen',
	'barre_inserer_code' => 'Einfügen, Rahmen und Färben des Code',
	'barre_quote' => '<quote>Nachricht</quote> zitieren',

	// C
	'classer' => 'Einsortieren',
	'clos' => 'Dieser Diskussionsstrang ist geschlossen.',

	// D
	'deplacer_dans' => 'Verschieben nach',
	'derniers' => 'Neuests Beiträge',
	'download' => 'Download der neuesten Version ',

	// F
	'forum_attention_explicite' => 'Dieser Titel ist nicht genau genug. Bitte präzisieren:', # MODIF
	'forum_invalide_titre' => 'Dieser Diskussionsstrang wurde deaktiviert',
	'forum_votre_email' => 'Ihre Mailadresse',

	// G
	'galaxie' => 'In der SPIP-Galaxis',

	// I
	'info_ajouter_document' => 'Sie können ihrem Beitrag ein Bildschirmfoto beifügen.',
	'info_connexion' => 'Eigene Beiträge können eine Stunde lang nachbearbeitet werden',
	'info_tag_forum' => 'Sie können diese Seite nach Belieben verschlagworten. Damit helfen Sie den nächsten Lesern bei der Orientierung.',
	'interetquestion' => 'Bitte beschreiben Sie den Grund Ihrer Frage',
	'interetreponse' => 'Bitte teilen Sie uns mit, ob die Antwort für Sie von Interesse war.',
	'inutile' => 'Nicht hilfreich',

	// L
	'liens_utiles' => 'Nützliche Links',
	'login_login2' => 'Login',

	// M
	'meme_sujet' => 'Zum gleichen Thema',
	'merci' => 'Danke',
	'messages' => 'Nachrichten',

	// N
	'navigationrapide' => 'Schnellzugriff:', # MODIF
	'nouvellequestion' => 'Neue Frage stellen',
	'nouvellereponse' => 'Auf diese Frage antworten',

	// P
	'page_utile' => 'Sie fanden diese Seite:',
	'par_date' => 'nach Datum',
	'par_interet' => 'nach Relevanz',
	'par_pertinence' => 'nach Übereinstimmung',

	// Q
	'questions' => 'Fragen',
	'quoideneuf' => 'Neue Einträge',

	// R
	'rechercher' => 'Suchen',
	'rechercher_forums' => 'In den Foren suchen',
	'rechercher_tout_site' => 'Ganze Website durchsuchen',
	'reponses' => 'Antworten',
	'resolu' => 'Erledigt',
	'resolu_afficher' => 'Beiträge mit Schlagwort « Erledigt » als erste anzeigen',
	'resolu_masquer' => 'Beiträge mit Schlagwort « Erledigt » ausblenden', # MODIF

	// S
	'suggestion' => 'Bevor sie weitermachen, sollten sie auf den folgenden Seiten nachsehen, ob dort eine Antwort auf ihre Frage gegeben wird.',
	'suivi_thread' => 'Diesen Diskussionsstrang syndizieren',

	// T
	'thememessage' => 'Thema dieses Forums:',
	'toutes_langues' => 'In allen Sprachen',
	'traductions' => 'Übersetzungen dieses Textes:',

	// U
	'utile' => 'Hilfreich'
);
