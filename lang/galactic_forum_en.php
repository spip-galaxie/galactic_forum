<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_forum?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_vote_inutile_question' => 'You disapproved of this question',
	'a_vote_inutile_reponse' => 'You disapproved of this answer',
	'a_vote_utile_question' => 'You approved this question',
	'a_vote_utile_reponse' => 'You approved this answer',
	'activite' => 'Activity',
	'activite_toutes_langues' => 'Multilingual activity ',
	'activite_toutes_langues_explication' => 'View of the activity whatever the language',
	'annees_depuis_nb' => 'Since @nb@ years',
	'annees_depuis_un' => 'Since 1 year',
	'annees_nb' => '@nb@ years',
	'annees_un' => '1 year',
	'aucun_message_mot' => 'This keyword is not attached to any message in this language.',
	'aucune_reponse' => 'No reply',
	'avertissement_code_forum' => 'To insert some code or ... ou mettre en valeur vos solutions, vous pouvez utiliser les raccourcis typographiques suivants :&lt;ul&gt;&lt;li&gt;&lt;code&gt;... une ou plusieurs lignes de code ...&lt;/code&gt;&lt;/li&gt;&lt;li&gt;&lt;cadre&gt;... code ayant des lignes très longues ...&lt;/cadre&gt;&lt;/li&gt;&lt;/ul&gt;',
	'avertissementforum' => '<b>P.S.</b> The forums of this site are vey active. Many thanks to those who animate and enrich these areas of mutual assistance.<p>However, the more the forums are active, the more dificult to follow and to consult they become. To turn these forums into a really exhilarating experience, we would be grateful if you follow these recommendations:<br /><img src=\'puce.gif\' border=\'0\' /> before starting a new discussion topic, please make sure that the subject has not been discussed here earlier;<br /><img src=\'puce.gif\' border=\'0\' /> make sure you ask your question in the section dedicated to it.',
	'avertissementtitre' => '<p>Make sure you give <strong>a clear title to your question</strong> in order to help other visitors navigate the forums.</p><p><strong>Messages without clear titles are deleted.</strong></p>',

	// B
	'barre_cadre_html' => 'Enclose and colour the <cadre class=\'html4strict\'>HTML code</cadre>',
	'barre_cadre_php' => 'Enclose and colour the <cadre class=\'php\'>PHP code</cadre>',
	'barre_cadre_spip' => 'Enclose and colour the <cadre class=\'spip\'>SPIP code</cadre>',
	'barre_code' => 'Insert <code>some code</code>',
	'barre_inserer_code' => 'Insert, enclose, and colour some code',
	'barre_quote' => 'Quote <quote>a message</quote>',

	// C
	'classer' => 'Sort',
	'classer_par_date' => 'Sort by date',
	'classer_par_note' => 'Sort by note',
	'clos' => 'This discussion thread is closed',
	'confirmer_spam_message' => 'Confirm that the message is spam? ',
	'confirmer_suppression_message' => 'Confirm deletion of this message?',
	'confirmer_suppression_mot' => 'Confirm the deletion of the word?',

	// D
	'date' => 'Date',
	'deplacer_dans' => 'Move to',
	'depuis_derniere_connexion' => 'Since your last login ',
	'depuis_origine' => 'Since the beginning',
	'dernier_message_dans_discussion' => 'Last post of this discussion',
	'derniere_connexion' => 'Last logon:',
	'derniers' => 'Latest messages',
	'derniers_messages' => 'Last posts',
	'derniers_messages_par_discussion' => 'Last posts by discussion',
	'derniers_sujets' => 'Last topics',
	'derniers_sujets_ouverts' => 'Last open topics',
	'derniers_sujets_vides' => 'Last unanswered topics ',
	'derniers_threads_par_date' => 'Threads by date of activity
',
	'discussions' => 'Discussions',
	'download' => 'Download the latest version',

	// E
	'en_un_an' => 'In one year:',
	'envoyer_message_a_auteur' => 'Send a message to @nom@',
	'envoyer_message_a_cet_auteur' => 'Send a message to this author',

	// F
	'facultatif' => 'optional',
	'faq' => 'FAQ',
	'faq_descriptif' => 'Topics solved best rated by the visitors',
	'forum_attention_explicite' => 'This title is not explicit enough, please detail it further:',
	'forum_invalide_titre' => 'This message thread has been invalidated',
	'forum_modere_titre' => 'This subject is waiting for validation',
	'forum_votre_email' => 'Your e-mail address (if you wish to receive the replies):',

	// G
	'galaxie' => 'In the SPIP universe',

	// I
	'info_ajouter_document' => 'You can attach a screen dump to your message',
	'info_connexion' => 'Allows you to edit your message for up to one hour',
	'info_ecrire_auteur' => 'You should be logged to send a private message:',
	'info_envoyer_message_prive' => 'allows to send a private message to the others registered contributors',
	'info_filtrer' => 'Filter',
	'info_nb_messages_a_valider' => '@nb@ posts to validate ',
	'info_nb_sujets_forum' => '@nb@ topics',
	'info_nb_sujets_forum_sur_un_an' => '@nb@ topics in one year',
	'info_nb_visiteurs_connectes' => '@nb@ visitors at the moment',
	'info_reponses_nb' => '@nb@ answers',
	'info_reponses_un' => '1 answer',
	'info_reponses_zero' => '0 answer',
	'info_resolues_pourcent' => '@pourcentage@ % solved',
	'info_resolus_pourcent' => '@pourcentage@ % solved',
	'info_tag_forum' => 'You can tag this forum page with the keywords that you think are the most appropriate. This will help future visitors to find answers more quickly.',
	'info_un_message_a_valider' => '1 message to validate',
	'info_un_sujet_forum' => '1 topic',
	'info_un_sujet_forum_sur_un_an' => '1 topic in one year',
	'info_un_visiteur_connecte' => '1 visitor at the moment ',
	'info_votes_moins_nb' => '@nb@ negative votes',
	'info_votes_moins_un' => '1 negative vote',
	'info_votes_moins_zero' => '0 negative vote',
	'info_votes_nb' => '@nb@ votes',
	'info_votes_plus_nb' => '@nb@ positives votes ',
	'info_votes_plus_un' => '1 positive vote ',
	'info_votes_plus_zero' => '0 positive vote',
	'info_votes_un' => '1 vote',
	'info_votes_zero' => '0 vote',
	'information_discussion_cloturee' => 'This discussion was closed by an administrator. It is not possible to add new comments.
',
	'information_edition_crayons' => 'You can edit your information with the pencils.
',
	'infos_stats_personnelles' => 'allows access to your personal login informations',
	'interet_question_inutile' => 'For you, this question is useless, unclear or shows no research effort',
	'interet_question_inutile_vote' => 'You indicated that this question was unhelpful, unclear or did not show any research effort ',
	'interet_question_utile' => 'For you, this question is useful, clear and shows a research effort.',
	'interet_question_utile_vote' => 'You indicated that this question was useful, clear and showed a research effort. ',
	'interet_reponse_inutile' => 'For you, this answer is useless',
	'interet_reponse_inutile_vote' => 'You indicated that this answer was useless',
	'interet_reponse_utile' => 'For you, this answer is useful',
	'interet_reponse_utile_vote' => 'You indicated that this answer was useful ',
	'interetquestion' => 'Please specify your interest in this question',
	'interetreponse' => 'Please specify your interest in this answer',
	'inutile' => 'useless',

	// L
	'liens_utiles' => 'Useful links',
	'login_login2' => 'Login',

	// M
	'ma_page' => 'My page',
	'ma_page_rediger_bio' => 'Fill in your bio... ',
	'ma_page_rediger_nom' => 'Fill in your name... ',
	'ma_page_rediger_site_web' => 'Fill in your site... ',
	'maj_date_activite' => 'Update your activity date',
	'maj_date_activite_explication' => 'The number of new messages is calculated from the date of activity. It is updated during authentication, or by clicking on this button. ',
	'meme_sujet' => 'On the same topic',
	'merci' => 'thank you',
	'message_aucun' => 'No message',
	'message_dans_discussion' => 'Message in the thread',
	'message_un' => '1 message',
	'messages' => 'messages',
	'messages_auteur' => 'Messages from this author:',
	'messages_connexion' => 'Messages since your last logon:',
	'messages_nb' => '@nb@ messages',
	'messages_un' => '1 message',
	'messages_zero' => '0 message',
	'mois_depuis_nb' => 'Since @nb@ month ',
	'mois_depuis_un' => 'Since 1 month ',
	'mois_nb' => '@nb@ month',
	'mois_un' => '1 month',

	// N
	'navigationrapide' => 'Quick navigation:',
	'nb_sujets_forum' => 'Topics',
	'nb_sujets_resolus' => 'Topics solved',
	'note' => 'Note',
	'notes' => 'Notes',
	'nouveaux_messages_connexion_nb' => '@nb@ new messages since your last connection 
',
	'nouveaux_messages_connexion_un' => '1 new message since your last connection 
',
	'nouveaux_messages_nb' => '@nb@ new messages',
	'nouveaux_messages_un' => '1 new message',
	'nouvellequestion' => 'Ask a new question',
	'nouvellereponse' => 'Answering the question',

	// P
	'page_utile' => 'Did you find this page:',
	'par_date' => 'by date',
	'par_date_naturelle' => 'By chronological date',
	'par_interet' => 'by rating',
	'par_note' => 'By note',
	'par_pertinence' => 'by relevancy',
	'participants' => 'Participants',
	'permalink' => 'Permanent link',

	// Q
	'questions' => 'Questions',
	'questions_activite_nb' => '@nb@ active questions',
	'questions_activite_un' => '1 active question',
	'questions_activite_zero' => '0 active question',
	'questions_et_derniere_reponse' => 'Questions and their latest answer',
	'questions_nb' => '@nb@ questions',
	'questions_ou_reponses' => 'Questions or Answers ',
	'questions_resolues' => 'Solved Questions',
	'questions_resolues_nb' => '@nb@ solved questions',
	'questions_resolues_un' => '1 solved question',
	'questions_resolues_zero' => '0 solved question',
	'questions_sans_reponse' => 'Unanswered Questions',
	'questions_sans_reponse_nb' => '@nb@ Unanswered Questions',
	'questions_sans_reponse_un' => '1 Unanswered Questions',
	'questions_sans_reponse_zero' => '0 Unanswered Questions',
	'questions_un' => '1 question',
	'questions_zero' => '0 question',
	'quoideneuf' => 'Recent changes',

	// R
	'rechercher' => 'Search',
	'rechercher_forums' => 'Search in forums',
	'rechercher_tout_site' => 'the whole site',
	'reponses' => 'Replies',
	'reponses_nb' => '@nb@ answers',
	'reponses_par_notes' => 'Answers sorted by notes',
	'reponses_un' => '1 Answer',
	'reponses_zero' => '0 Answer',
	'resolu' => 'Solved',
	'resolu_afficher' => 'Display only the messages linked to the keyword «solved»',
	'resolu_masquer' => 'Display all the results',
	'resolu_non' => 'Unresolved',
	'resolues' => 'Solved',
	'resolus' => 'Solved',

	// S
	'sans_limite' => 'Without limit',
	'sans_reponse' => 'No answer',
	'sans_reponses' => 'No answers',
	'statistiques' => 'Statistics',
	'statistiques_auteur' => 'Author’s statistics',
	'statistiques_categorie' => 'Category Statistics',
	'statut' => 'Status:',
	'suggestion' => 'Before continuing, have you consulted the following pages? Perhaps they might provide with the answer you seek.',
	'suivi_thread' => 'Syndicate this thread',
	'sujets' => 'Topics',
	'sujets_auteur' => 'Topics by this author:',
	'sujets_par_notes' => 'Topics sorted by number of notes',
	'sujets_resolus_par_notes' => 'Solved topics, sorted by number of notes',

	// T
	'thememessage' => 'This forum’s theme:',
	'toutes_langues' => 'In all languages',
	'traductions' => 'Translations of this text:',

	// U
	'utile' => 'useful'
);
