<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip(_DIR_PLUGIN_FORUM . 'formulaires/forum');

function formulaires_forum_charger(
	$objet,
	$id_objet,
	$id_forum,
	$ajouter_mot,
	$ajouter_groupe,
	$forcer_previsu,
	$retour
) {
	$valeurs = formulaires_forum_charger_dist(
		$objet,
		$id_objet,
		$id_forum,
		$ajouter_mot,
		$ajouter_groupe,
		$forcer_previsu,
		$retour
	);

	// à la création d’un sujet, ne pas proposer le titre de l’article !
	if (empty($valeurs['id_forum'])) {
		$valeurs['titre'] = '';
	}

	return $valeurs;
}