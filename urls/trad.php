<?php

// executer une seule fois
if (defined("_INC_URLS2")) return;
define("_INC_URLS2", "1");

// attention maintenant ces fichiers sont appeles depuis l'espace prive aussi...
function urls_trad_langue_choix($id, $type = 'article') {
	switch ($type) {
		case 'article':
			$a = sql_fetsel("lang, id_secteur", "spip_articles", "id_article=".intval($id));
			if ($a) {
				# aide en ligne
				if ($a['id_secteur'] == 324) {
					return "aide/" . $a['lang'] . '-aide.html';
				}
#					if ($a['id_secteur'] == 91)  # rubrique 'fr'
#						return 'fr';
#					if ($a['lang'] <> 'fr')      # rubrique 'traducteurs'
				return preg_replace("/_.*/", "", $a['lang']);
#					else
#						return '';
			}
			break;
		case 'rubrique':
			$a = sql_fetsel("lang, id_secteur", "spip_rubriques", "id_rubrique=".intval($id));
			if ($a) {
				return preg_replace("/_.*/", "", $a['lang']);
			}
			break;
		case 'forum':
			include_spip('inc/forum');
			if ($racine = racine_forum($id)) {
				return urls_trad_langue_choix($racine[1], $racine[0]);
			}
			break;
	}
	// cas par defaut en cas d'echec ?
	return 'fr';
}

function urls_trad_generer_url_article($id_article) {
	$lang = urls_trad_langue_choix($id_article);
	if (preg_match('#aide/#', $lang)) {
		return $lang;
	} elseif ($lang) {
		return $lang . "_article$id_article.html";
	} else {
		return "article$id_article.html";
	}
}

function urls_trad_generer_url_rubrique($id_rubrique) {
	#   $id_secteur = sql_getfetsel("id_secteur", "spip_rubriques", "id_rubrique=".intval($id_rubrique)." AND id_secteur=id_rubrique");
	#	if ($id_secteur AND $url=langue_choix ($id_secteur, 'rubrique'))
	#		return $url;
	if ($lang = urls_trad_langue_choix($id_rubrique, 'rubrique')) {
		return $lang . "_rubrique$id_rubrique.html";
	}
	return "rubrique$id_rubrique.html";
}

function urls_trad_generer_url_breve($id_breve) {
	return "breve$id_breve.html";
}

function urls_trad_generer_url_forum($id_forum) {
	$t = sql_fetsel(
		array('id_thread', 'id_forum'),
		'spip_forum',
		array('id_forum=' . intval($id_forum))
	);
	$lang = urls_trad_langue_choix($id_forum, 'forum');
	if ($t) {
		$url = $lang . "_" . $t['id_thread'] . '.html';
		if ($t['id_forum'] <> $t['id_thread']) {
			$url .= '#forum' . $t['id_forum'];
			$url = parametre_url($url, 'debut_forums', '@' . $id_forum);
		}
		return $url;
	}
	return '';
}

function urls_trad_generer_url_mot($id_mot) {
	include_spip('inc/charsets');
	$titre = sql_getfetsel("titre", "spip_mots", "id_mot=".intval($id_mot));
	if ($titre) {
		$url = '@' . preg_replace('/[^a-z0-9_,-]/', '',
				strtolower(translitteration($titre)));
		$extra = serialize(array('url' => $url));
		sql_updateq("spip_mots", ['extra' => $extra], "id_mot=".intval($id_mot));
		return $url;
	}
	return "mot$id_mot.html";
}

function urls_trad_generer_url_auteur($id_auteur) {
	return "auteur$id_auteur.html";
}

function urls_trad_generer_url_document($id_document) {
	if ($id_document > 0) {
		$row = sql_fetsel("fichier", "spip_documents", "id_document=".intval($id_document));
		if ($row) {
			include_spip('inc/documents');
			$url = get_spip_doc($row['fichier']);
		}
	}
	return "document$id_document.html";
}

// legacy spip <=4.0
function urls_trad_dist($i, $entite, $args = '', $ancre = '') {
	if (is_numeric($i)) {
		return urls_propres_generer_url_objet_dist(intval($i), strval($entite), $args, $ancre);
	}

	$contexte = [];
	if ($args) {
		parse_str($args, $contexte);
	}
	return urls_trad_decoder_url_dist(strval($i), $entite, $contexte);
}

/**
 * Generer l'url d'un objet SPIP
 * @param int $id
 * @param string $objet
 * @param string $args
 * @param string $ancre
 * @return string
 */
function urls_propres_generer_url_objet_dist(int $id, string $objet, string $args = '', string $ancre = ''): string {
	if (function_exists("urls_trad_generer_url_$objet")) {
		$generer_url_entite = "urls_trad_generer_url_$objet";
		$url = $generer_url_entite($id);
	}
	else {
		$url = "{$objet}{$id}.html";
	}

	// Ajouter les args
	if ($args) {
		$url .= ((strpos($url, '?') === false) ? '?' : '&') . $args;
	}

	// Ajouter l'ancre
	if ($ancre) {
		$url .= "#$ancre";
	}

	return $url;
}

/**
 * Decoder une url trad
 * retrouve le fond et les parametres d'une URL abregee
 * le contexte deja existant est fourni dans args sous forme de tableau
 *
 * @param string $url
 * @param string $entite
 * @param array $contexte
 * @return array([contexte],[type],[url_redirect],[fond]) : url decodee
 */
function urls_trad_decoder_url_dist(string $url, string $entite, array $contexte = []): array {
	$url_redirect = null;

	// Chercher les valeurs d'environnement qui indiquent l'url-propre
	$url_propre = preg_replace(',[?].*,', '', $url);
	$url_propre = preg_replace(',\.html$,i', '', $url_propre);

	// le cas particulier des mots cles
	if (preg_match("#^(@[a-z_0-9,-]+)(\?.*)?$#", $url, $regs)) {
		$extra = serialize(array('url' => $regs[1]));
		if ($id_mot = sql_getfetsel("id_mot", "spip_mots", "extra=".sql_quote($extra))) {
			$entite = 'mot';
			$contexte['id_mot'] = $id_mot;
		}
	}
	elseif (preg_match(",^([a-z_]*)?(article|rubrique|breve|forum|document|auteur)(0-9]+)$,i", $url_propre, $match)) {
		if ($objet = $match[2] and $id_objet = intval($match[3])) {
			$entite = $objet;
			if (!empty($match[1])) {
				//$contexte['lang'] = $match[1];
			}
			$contexte[id_table_objet($entite)] = $id_objet;
		}
	}
	return [$contexte, $entite, $url_redirect, null];
}

