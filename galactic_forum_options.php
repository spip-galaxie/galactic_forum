<?php
// Pour tester : envoi des mails sur les threads
// cf. inc/notifications.php
define('_SUIVI_FORUM_THREAD', true);

include_spip('urls/trad');

// URLs de la forme /fr_article1.html (inspire de www.spip.net)
$GLOBALS['type_urls'] = 'trad';

// Ajouter la barre typo speciale (<code> et <cadre>)
$GLOBALS['barre_typo'] = 'forumspiporg';

// longueur mini message
if (!defined('_FORUM_LONGUEUR_MINI')) {
	define('_FORUM_LONGUEUR_MINI', 3);
}
